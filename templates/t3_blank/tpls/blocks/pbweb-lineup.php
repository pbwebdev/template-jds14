<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<div class="wrap t3-sl t3-sl-1 lineup">
    <div class="container">

        <p>Full day Joomla conference, full day hands on workshops,<br/>two main streams, social events & networking + lots more...</p>

    </div>
</div>