<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php if ($this->checkSpotlight('silver', 'silver-1, silver-2, silver-3')) : ?>
<!-- SPOTLIGHT 1 -->
<div class="container t3-sl t3-sl-1 silver">
  <?php 
  	$this->spotlight ('silver', 'silver-1, silver-2, silver-3')
  ?>
</div>
<!-- //SPOTLIGHT 1 -->
<?php endif ?>