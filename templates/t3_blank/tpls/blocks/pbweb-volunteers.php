<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<!-- volunteer 1 -->
<div class="wrap t3-sl t3-sl-1 volunteers">
    <div class="container">
        <jdoc:include type="modules" name="volunteers" style="xhtml" />
    </div>
</div>
<!-- //volunteer 1 -->
