<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php if ($this->checkSpotlight('speakers', 'speakers-1, speakers-2, speakers-3, speakers-4')) : ?>
<!-- speakers 1 -->
<div id="speakers" class="wrap t3-sl t3-sl-1 speakers">
    <div class="container">
        <h2>Call for Speaker Sessions</h2>
      <?php
        $this->spotlight ('speakers', 'speakers-1, speakers-2, speakers-3, speakers-4')
      ?>

<!--        <a class="btn btn-info text-center" href="index.php?option=com_content&view=category&layout=blog&id=13&Itemid=161">Learn more about the speakers</a>-->


  </div>
</div>
<!-- //speakers 1 -->
<?php endif ?>