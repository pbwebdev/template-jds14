<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php if ($this->checkSpotlight('bronze', 'bronze-1, bronze-2, bronze-3, bronze-4')) : ?>
<!-- bronze 1 -->
<div id="bronze" class="wrap t3-sl t3-sl-1 bronze">
    <div class="container">
        <h2>Thanks to Our Bronze Sponsors</h2>
            <?php
              $this->spotlight ('bronze', 'bronze-1, bronze-2, bronze-3, bronze-4')
            ?>
    </div>

</div>
<!-- //bronze 1 -->
<?php endif ?>