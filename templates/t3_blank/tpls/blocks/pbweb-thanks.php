<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php if ($this->checkSpotlight('thanks', 'thanks-1, thanks-2, thanks-3')) : ?>
<!-- thanks 1 -->
<div class="wrap t3-sl t3-sl-1 specialthanks">
    <div class="container">
        <h2>Special thanks to</h2>
          <?php
            $this->spotlight ('thanks', 'thanks-1, thanks-2, thanks-3')
          ?>
    </div>
</div>
<!-- //thanks 1 -->
<?php endif ?>