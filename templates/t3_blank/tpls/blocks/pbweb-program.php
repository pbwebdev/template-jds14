<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php if ($this->checkSpotlight('program', 'program-1, program-2')) : ?>
<!-- SPOTLIGHT 1 -->
<div id="program" class="wrap t3-sl t3-sl-1 program">
    <div class="container">
        <h2>Full Day Program</h2>
  <?php 
  	$this->spotlight ('program', 'program-1, program-2')
  ?>

        <center><a class="btn btn-info text-center" href="index.php?option=com_content&view=category&layout=blog&id=12&Itemid=162">View the full workshop & presentations programs</a>
        </center>

    </div>
</div>
<!-- //SPOTLIGHT 1 -->
<?php endif ?>