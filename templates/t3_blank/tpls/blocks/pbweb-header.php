<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$sitename = $this->params->get('sitename') ? $this->params->get('sitename') : JFactory::getConfig()->get('sitename');
$slogan = $this->params->get('slogan');
$logotype = $this->params->get('logotype', 'text');
$logoimage = $logotype == 'image' ? $this->params->get('logoimage', '') : '';
if ($logoimage) {
  $logoimage = ' style="background-image:url('.JURI::base(true).'/'.$logoimage.');"';
}
?>

<!-- HEADER -->
<header id="t3-header" class="wrap t3-header">
<div class="container">
  <div class="row">

    <!-- LOGO -->
    <div class="span7 logo hidden-phone">
      <div class="logo-<?php echo $logotype ?>">
        <a href="<?php echo JURI::base(true) ?>" title="<?php echo strip_tags($sitename) ?>"<?php echo $logoimage ?>>
          <span><?php echo $sitename ?></span>
        </a>
        <h1>JoomlaDay Sydney 2014</h1>
        <p>October 11th - 12th 2014</p>
      </div>
    </div>
    <!-- //LOGO -->

    <div class="span4 clearfix">
        <div class="topRight">
            <jdoc:include type="modules" name="topRight" style="xhtml" />
        </div>

        <div class="sponsor-gold">
            <jdoc:include type="modules" name="sponsor-gold" style="xhtml" />
        </div>

    </div>

  </div>
</div>
</header>
<!-- //HEADER -->
