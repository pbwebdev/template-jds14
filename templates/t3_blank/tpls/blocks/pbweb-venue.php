<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php if ($this->checkSpotlight('venue', 'venue-1, venue-2')) : ?>
<!-- SPOTLIGHT 1 -->
<div id="venue" class="wrap t3-sl t3-sl-1 venue">
    <div class="container">
  <h2>Venue</h2>
        <?php
  	$this->spotlight ('venue', 'venue-1, venue-2')
  ?>

        <center><a class="btn btn-info text-center" href="index.php?option=com_content&view=category&layout=blog&id=14&Itemid=164">Find out more about the venue</a>
        </center>

    </div>
</div>
<!-- //SPOTLIGHT 1 -->
<?php endif ?>