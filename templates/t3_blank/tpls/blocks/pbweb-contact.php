<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php if ($this->checkSpotlight('contact', 'contact-1, contact-2')) : ?>
<!-- SPOTLIGHT 1 -->
<div id="contact" class="wrap t3-sl t3-sl-1 contact">
    <div class="container">
        <h2>Contact Us</h2>
          <?php
            $this->spotlight ('contact', 'contact-1, contact-2')
          ?>
    </div>
</div>
<!-- //SPOTLIGHT 1 -->
<?php endif ?>

