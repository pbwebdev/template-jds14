<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="wrap t3-sl t3-sl-1 lineup">
    <div class="container">

        <p>Become a fan on <a href="https://www.facebook.com/JoomlaDaySyd" target="_blank">Facebook</a><br>and follow us on <a href="https://twitter.com/JoomlaDaySydney" target="_blank">Twitter</a></p>

    </div>
</div>