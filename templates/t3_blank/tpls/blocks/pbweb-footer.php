<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<!-- FOOTER -->
<footer id="t3-footer" class="wrap t3-footer">

  <!-- FOOT NAVIGATION -->
  <div class="container">
    <?php $this->spotlight ('footnav', 'footer-1, footer-2, footer-3, footer-4, footer-5, footer-6') ?>
  </div>
  <!-- //FOOT NAVIGATION -->

  <section class="t3-copyright">
    <div class="container">
      <div class="row">
        <div class=" copyright<?php $this->_c('footer')?>">
            <div class="span8">
                <jdoc:include type="modules" name="<?php $this->_p('footer') ?>" />
            </div>
            <div class="span4 links">
                <ul>
                    <li>
                        <a href="http://joomla.org" target="_blank" title="Joomla - Open Source Content Management System">Built with love on Joomla</a>
                    </li>
                    <li>
                        <a href="http://www.meetup.com/Joomla-User-Group-Sydney/" target="_blank" title="Sydney web design agency">Collaboration with the Sydney Joomla Meetup</a>
                    </li>
                    <li>
                        <a href="http://pbwebdev.com" target="_blank" title="Sydney web design agency">Website by PB Web Development</a>
                    </li>
                </ul>
            </div>
        </div>
      </div>
    </div>
  </section>

</footer>
<!-- //FOOTER -->