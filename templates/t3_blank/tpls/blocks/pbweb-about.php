<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php if ($this->checkSpotlight('about-1', 'about-1, about-2')) : ?>
<!-- SPOTLIGHT 1 -->
<div class="wrap t3-sl t3-sl-1 about">
    <div class="container">

        <h2>About JoomlaDay Sydney 2014</h2>

  <?php 
  	$this->spotlight ('about-1', 'about-1, about-2')
  ?>
    </div>
</div>
<!-- //SPOTLIGHT 1 -->
<?php endif ?>