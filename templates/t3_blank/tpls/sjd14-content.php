<?php
/** 
 *------------------------------------------------------------------------------
 * @package       T3 Framework for Joomla!
 *------------------------------------------------------------------------------
 * @copyright     Copyright (C) 2004-2013 JoomlArt.com. All Rights Reserved.
 * @license       GNU General Public License version 2 or later; see LICENSE.txt
 * @authors       JoomlArt, JoomlaBamboo, (contribute to this project at github 
 *                & Google group to become co-author)
 * @Google group: https://groups.google.com/forum/#!forum/t3fw
 * @Link:         http://t3-framework.org 
 *------------------------------------------------------------------------------
 */


defined('_JEXEC') or die;
?>

<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" class='<jdoc:include type="pageclass" />'>

  <head>
    <jdoc:include type="head" />
    <?php $this->loadBlock ('pbweb-head') ?>
  </head>

  <body id="top">

  <a id="back-top" class="jump" href="#top" style="display: inline;"><i class="icon-chevron-up"></i></a>

  <?php $this->loadBlock ('pbweb-header') ?>
    <?php $this->loadBlock ('pbweb-mainnav') ?>

    <?php $this->loadBlock ('pbweb-mainbody-content-left') ?>


    <?php $this->loadBlock ('pbweb-bronze') ?>
    <?php $this->loadBlock ('pbweb-volunteers') ?>
  <?php $this->loadBlock ('pbweb-navhelper') ?>
    <?php $this->loadBlock ('pbweb-footer') ?>
    
  </body>

</html>