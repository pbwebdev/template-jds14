/** 
 *------------------------------------------------------------------------------
 * @package       T3 Framework for Joomla!
 *------------------------------------------------------------------------------
 * @copyright     Copyright (C) 2004-2013 JoomlArt.com. All Rights Reserved.
 * @license       GNU General Public License version 2 or later; see LICENSE.txt
 * @authors       JoomlArt, JoomlaBamboo, (contribute to this project at github 
 *                & Google group to become co-author)
 * @Google group: https://groups.google.com/forum/#!forum/t3fw
 * @Link:         http://t3-framework.org 
 *------------------------------------------------------------------------------
 */
/**
 * @version		$Id: k2.js 1987 2013-06-27 11:51:59Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2013 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

var $sjd13 = jQuery.noConflict();

$sjd13(document).ready(function(){



    // Smooth Scroll
    $sjd13('.jump').click(function(event){
        event.preventDefault();
        var target = this.hash;
        $sjd13('html, body').stop().animate({
            scrollTop: $sjd13(target).offset().top
        }, 1000);
    });

    $sjd13(".menustick").sticky({topSpacing:0});


});
